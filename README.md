# gps-tracker-format-explanation

### Explanation of a sample string

```$aaccc,c–c*hh<CR><LF>```

| **Field**     | **Description**                                                                   |
| ---------     | --------------------------------------------------------------------------------- |
| $             | Start of sentence                                                                 |
| aaccc         | Address field. “aa” is the talker identifier. “ccc” identifies the sentence type. Talker IDs can be GP and GN when the message ID is RMC, GSA or GLL, and Talker IDs can be GP and GL when the message ID is GSV. Otherwise, Talker ID is always GP |
| ,             | Field delimiter                                                                   |
| c-c           | Data sentence block                                                               |
| \*            | Checksum delimiter                                                                |
| hh            | Checksum field                                                                    |
| \<CR>\<LF>    | Ending of sentence. (Carriage return, line feed)                                  |

Checksum field is the 8-bit exclusive OR (no start or stop bits) of all characters in the sentence. Checksum consists of 2
characters and is represented as a hex number. 

### Data sentence block

The formats are described as follows:

**GGA – Global Positioning System Fix Data**

Time, position and fix related data for a GPS receiver. 

Format:

```$--GGA,hhmmss.ss,llll.lll,A,yyyyy.yyy,A,x,uu,v.v,w.w,M,x.x,M,,zzzz*hh<CR><LF>```

| **Field** | **Description**                                                                                                                   |
| --------- | --------------------------------------------------------------------------------------------------------------------------------- |
| hhmmss.ss | UTC of position in hhmmss.sss format, (000000.000 ~ 235959.999)                                                                   |
| llll.lll  | Latitude in ddmm.mmmm format. Leading zeros are inserted.                                                                         |
| A         | N/S Indicator ‘N’ = North, ‘S’ = South.                                                                                           |
| yyyyy.yyy | Longitude in dddmm.mmmm format. Leading zeros are inserted.                                                                       |
| A         | E/W Indicator ‘E’ = East, ‘W’ = West.                                                                                             |
| x         | GPS quality indicator. 0: position fix unavailable. 1: valid position fix, SPS mode. 2: valid position fix, differential GPS mode, 4: Real-Time Kinematic, fixed integers, 5: Real-Time Kinematic, float integers, OmniSTAR XP/HP or Location RTK                    |
| uu        | Number of satellites in use, (00 ~ 24)                                                                                            |
| v.v       | HDOP Horizontal dilution of precision, (00.0 ~ 99.9)                                                                              |
| w.w       | Mean sea level altitude (-9999.9 ~ 17999.9) in meter                                                                              |
| x.x       | Geoidal Separation in meter                                                                                                       |
| zzzz      | DGPS Station ID Differential reference station ID, 0000 ~ 1023. NULL when DGPS not used                                           |
| hh        | Checksum                                                                                                                          |

Example:

```$GNGGA,122444.00,6015.52586,N,02450.67823,E,5,12,1.05,38.4,M,17.7,M,1.0,3581*5F```

**GLL – Geographic Position – Latitude/Longitude**

Latitude and longitude of vessel position, time of position fix and status.

Format:

```$--GLL,llll.lll,a,yyyyy.yyy,b,hhmmss.sss,A,a*hh<CR><LF>```

| **Field**     | **Description**                                                                                                                  |
| ------------- | -------------------------------------------------------------------------------------------------------------------------------- |
| llll.lll      | Latitude in ddmm.mmmm format. Leading zeros are inserted                                                                         |
| A             | N/S Indicator ‘N’ = North, ‘S’ = South                                                                                           |
| yyyyy.yyy     | Longitude in dddmm.mmmm format. Leading zeros are inserted                                                                       |
| B             | E/W Indicator ‘E’ = East, ‘W’ = West                                                                                             |
| hhmmss.sss    | UTC of position in hhmmss.sss format, (000000.000 ~ 235959.999)                                                                  |
| A             | Status A= data valid, V= Data not valid.                                                                                         |
| hh            | Checksum                                                                                                                         |

*Example:*

```$GNGLL,6015.52586,N,02450.67823,E,122444.00,A,D*72```

**GSA – GNSS DOP and Active Satellites**

GPS receiver operating mode, satellites used in the navigation solution reported by the GGA or GNS sentence and DOP
values.

Format:

```$--GSA,a,x,xx,xx,xx,xx,xx,xx,xx,xx,xx,xx,xx,xx,u.u,v.v,z.z*hh<CR><LF>```

| **Field**     | **Description**                                                                                                                  |
| ------------- | -------------------------------------------------------------------------------------------------------------------------------- |
| a             | Mode. ‘M’ = Manual, forced to operate in 2D or 3D mode. ‘A’ = Automatic, allowed to automatically switch 2D/3D                   |
| x             | Mode Fix type. 1 = Fix not available. 2 = 2D. 3 = 3D                                                                             |
| xx’s          | Satellite ID 01 ~ 32 are for GPS; 33 ~ 64 are for SBAS (PRN minus 87); 65 ~ 96 are for GLONASS (64 plus slot numbers); 193 ~ 197 are for QZSS; 01 ~ 37 are for Beidou (BD PRN). GPS and Beidou satellites are differentiated by the GP and BD prefix. Maximally 12 satellites are included in each GSA sentence.             |
|u.u            | PDOP Position dilution of precision (00.0 to 99.9)                                                                               |
| v.v           | HDOP Horizontal dilution of precision (00.0 to 99.9)                                                                             |
| z.z           | VDOP Vertical dilution of precision (00.0 to 99.9)                                                                               |
| hh            | Checksum                                                                                                                         |

*Example:*

```$GNGSA,A,3,10,32,14,11,08,01,22,17,24,,,,2.02,1.05,1.73,1*0C```

**GSV – GNSS Satellites in View**

Number of satellites (SV) in view, satellite ID numbers, elevation, azimuth, and SNR value. Four satellites maximum per
transmission.

Format:

```$--GSV,x,u,xx,uu,vv,zzz,ss,uu,vv,zzz,ss,…,uu,vv,zzz,ss*hh<CR><LF>```

| **Field**     | **Description**                                                                                                                  |
| ------------- | -------------------------------------------------------------------------------------------------------------------------------- |
| x             | Total number of GSV messages to be transmitted (1-3)                                                                             |
| u             | Sequence number of current GSV message                                                                                           |
| xx            | Total number of satellites in view (00 ~ 12)                                                                                     |               
| uu            | Satellite ID 01 ~ 32 are for GPS; 33 ~ 64 are for SBAS (PRN minus 87); 65 ~ 96 are for GLONASS (64 plus slot numbers); 193 ~ 197 are for QZSS; 01 ~ 37 are for Beidou (BDPRN). GPS and Beidou satellites are differentiated by the GP and BD prefix. Maximally 4 satellites are included in each GSV sentence.           |
| Vv            | Elevation Satellite elevation in degrees, (00 ~ 90)                                                                              |
| zzz           | Azimuth Satellite azimuth angle in degrees, (000 ~ 359 )                                                                         |
| ss            | SNR C/No in dB (00 ~ 99). Null when not tracking                                                                                 |
| hh            | Checksum                                                                                                                         |

*Example:*

```$GLGSV,3,2,10,75,00,302,,76,19,344,27,77,18,040,25,83,21,153,25,3*74```
```$GAGSV,3,2,09,24,14,329,21,25,12,018,28,26,64,176,33,31,00,283,,7*79```

**RMC – Recommended Minimum Specific GNSS Data**

Time, date, position, course and speed data provided by a GNSS navigation receiver.

Format:

```$--RMC,hhmmss.sss,x,llll.lll,a,yyyyy.yyy,a,x.x,u.u,xxxxxx,,,A,v*hh<CR><LF>```

| **Field**     | **Description**                                                                                                                  |
| ------------- | -------------------------------------------------------------------------------------------------------------------------------- |
| hhmmss.sss    | UTC time in hhmmss.sss format (000000.000 ~ 235959.999)                                                                          |
| x             | Status. ‘V’ = Navigation receiver warning. ‘A’ = Data Valid.                                                                     |
| llll.lll      | Latitude in dddmm.mmmm format. Leading zeros are inserted.                                                                       |
| A             | N/S indicator ‘N’ = North; ‘S’ = South                                                                                           |
| yyyyy.yyy     | Longitude in dddmm.mmmm format. Leading zeros are inserted.                                                                      |
| A             | E/W Indicator ‘E’ = East; ‘W’ = West.                                                                                            |
| x.x           | Speed over ground in knots (000.0 ~ 999.9)                                                                                       |
| u.u           | Course over ground in degrees (000.0 ~ 359.9)                                                                                    |
| xxxxxx        | UTC date of position fix, ddmmyy format                                                                                          |
| A             | Mode indicator. ‘A’ = Autonomous mode, ‘D’ = Differential mode, ‘E’ = Estimated (dead reckoning) mode, F = RTK Float mode, ´M´ = Manual input, ‘N’ = Data not valid, ´P´ = Precise (4.00 and later), ´R´ = RTK Integer mode, ´S´ = Simulated Mode, ´V´ = Navigational status not valid. NOTE: This field may be empty. In pre-2.3 versions it is omitted. [NTUM] says that according to the NMEA specification, it dominates the Status field — the Status field will be set to "A" (data valid) for Mode Indicators A and D, and to "V" (data invalid) for all other values of the Mode Indicator. This is confirmed by [IEC].                                                                   |
| v             | Navigational status indicator: V (Equipment is not providing navigational status information, fixed field, only available in NMEA 4.10 and later). For ublox this is a fixed value "V".                                                                                                                                 | 
| hh            | checksum                                                                                                                         |

*Example:*

```$GNRMC,122445.00,A,6015.52587,N,02450.67818,E,0.013,,030919,,,F,V*1B```

**VTG – Course Over Ground and Ground Speed**

The actual course and speed relative to the ground.

Format:

```$--VTG,x.x,T,y.y,M,u.u,N,v.v,K,m*hh<CR><LF>```

| **Field**     | **Description**                                                                                                                  |
| ------------- | -------------------------------------------------------------------------------------------------------------------------------- |
| x.x           | Course over ground, degrees True (000.0 ~ 359.9)                                                                                 |
| y.y           | Course over ground, degrees Magnetic (000.0 ~ 359.9)                                                                             |
| u.u           | Speed over ground in knots (000.0 ~ 999.9)                                                                                       |
| v.v           | Speed over ground in kilometers per hour (0000.0 ~ 1800.0)                                                                       |
| m             | Mode indicator.‘N’ = not valid, ‘A’ = Autonomous mode, ‘D’ = Differential mode, ‘E’ = Estimated (dead reckoning) mode            |
| hh            | Checksum                                                                                                                         |

*Example:*

```$GNVTG,,T,,M,0.027,N,0.049,K,D*30```

**ZDA – Time and Date**

UTC, day, month, year and local time zone.

Format:

```$--ZDA,hhmmss.sss,dd,mm,yyyy,xx,yy*hh<CR><LF>```

| **Field**     | **Description**                                                                                                                   |
| ------------- | --------------------------------------------------------------------------------------------------------------------------------- |
| hhmmss.sss    | UTC time in hhmmss.sss format (000000.000 ~ 235959.999)                                                                           |
| dd            | UTC day 01 to 31                                                                                                                  |
| mm            | UTC month 01 to 12                                                                                                                |
| yyyy          | UTC year Four-digit year number                                                                                                   |
| xx            | Local zone hours 00 to +-13                                                                                                       |
| yy            | Local zone minutes 00 to +59                                                                                                      |
| hh            | Checksum                                                                                                                          |

*Example:*
Not observed in current device.

[Source](http://navspark.mybigcommerce.com/content/NMEA_Format_v0.1.pdf)

[GPS Quality Indicator](https://www.trimble.com/oem_receiverhelp/v4.44/en/nmea-0183messages_gga.html)
[Mode indicator for RMC](https://gpsd.gitlab.io/gpsd/NMEA.html)
[V as mode indicator for RMC](https://www.quectel.com/UploadImage/Downlad/Quectel_L26_GNSS_Protocol_Specification_V1.4.pdf)
[Talker ID explanation](https://www.quectel.com/UploadImage/Downlad/Quectel_L26_GNSS_Protocol_Specification_V1.4.pdf)
[ublox](https://www.u-blox.com/sites/default/files/products/documents/u-blox8-M8_ReceiverDescrProtSpec_(UBX-13003221)_Public.pdf)